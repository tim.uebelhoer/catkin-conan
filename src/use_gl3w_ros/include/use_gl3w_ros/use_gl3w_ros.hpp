#pragma once
#include <gl3w_ros/gl3w_ros.hpp>

namespace use_gl3w_ros
{
class UseGl3wRos
{
public:
  /*!
  \returns EXIT_SUCCESS if gl3w has been found and a context could be created
  */
  int test();

private:
  gl3w_ros::Gl3wRos gl_ros;
};
} // namespace use_gl3w_ros