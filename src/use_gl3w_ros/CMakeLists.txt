cmake_minimum_required(VERSION 2.8.3)
project(use_gl3w_ros)
add_compile_options(-std=c++11)

#######################
## Find Dependencies ##
#######################

find_package(catkin REQUIRED COMPONENTS
  gl3w_ros)

#############
## Package ##
#############

catkin_package(
 INCLUDE_DIRS include
 LIBRARIES use_gl3w_ros
 CATKIN_DEPENDS gl3w_ros)

###########
## Build ##
###########

add_library(${PROJECT_NAME})
target_sources(${PROJECT_NAME} PRIVATE
  src/use_gl3w_ros.cpp)
target_include_directories(${PROJECT_NAME} PUBLIC
  include
  ${catkin_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PUBLIC
  ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}_node)
target_sources(${PROJECT_NAME}_node PRIVATE
  src/use_gl3w_ros_node.cpp)
target_link_libraries(${PROJECT_NAME}_node PRIVATE
  ${PROJECT_NAME})

#############
## Install ##
#############

# Mark executables and/or libraries for installation
install(TARGETS ${PROJECT_NAME} ${PROJECT_NAME}_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

# Mark cpp header files for installation
install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.hpp"
)
