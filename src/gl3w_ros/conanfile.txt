[requires]
gl3w/0.2@tuebel/testing 
glfw/3.2.1@bincrafters/stable

[generators]
cmake_find_package

[options]
*:shared=True