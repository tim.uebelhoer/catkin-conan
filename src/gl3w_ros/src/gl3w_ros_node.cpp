#include <gl3w_ros/gl3w_ros.hpp>
#include <ros/ros.h>

/*!
Simple application that only prints if gl3w has been loaded
*/
int main(int argc, char **argv)
{
  ros::init(argc, argv, "gl3w_ros_node");
  ros::NodeHandle node_handle;
  gl3w_ros::Gl3wRos gl_ros;
  int success = gl_ros.test();
  if(success == EXIT_SUCCESS){
    ROS_INFO("Succeeded to load OpenGL");
  }else{
    ROS_ERROR("Failed to load OpenGL");
  }
  return success;
}
