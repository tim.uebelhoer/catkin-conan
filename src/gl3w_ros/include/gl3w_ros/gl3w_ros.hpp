#pragma once

namespace gl3w_ros
{
class Gl3wRos
{
public:
  /*!
  \returns EXIT_SUCCESS if gl3w has been found and a context could be created
  */
  int test();
};
} // namespace gl3w_ros